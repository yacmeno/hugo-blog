---
title: "Playing with React Hooks"
tags: ["React", "Web"]
---

# Intro

React is my go-to front-end library for web development when vanilla JS is not good enough. With React 16.8 came the introduction of hooks, which allow you to more easily write components in a functional way. I used a side project to learn about the new concepts introduced, and other different web APIs. The side project is a single-page app that consumes the [TMDB API](https://www.themoviedb.org/) and explores the following things:
- React hooks + custom hooks with Typescript
- Browsers' IndexedDB and offline storage
- Homemade client-side routing with the browser history API (no lib like React Router)
- Homemade project config (e.g. webpack, eslint, tsconfig, scss, etc.)

Briefly, the app allows you to: search for movies by title, browse the current popular movies and save them to a watch later list (which works offline). The app is freely deployed on [Netlify](https://tmdbapp.netlify.com/) and available on [github](https://github.com/yacmeno/tmdb-app).

## On hooks

I quite like the addition of hooks. In the totality of the project, I did not need to resort to using the class-based React API. I have nothing against classes, but I believe React is more fun to use with the functional paradigm (it's also significantly pushed from the devs imo).

Throughout the project, I got to use the following hooks: useState, useEffect, useContext, useCallback, useReducer. useState and useEffect are used very often and are easy to understand, so I won't really get into why/how I used them.

### useContext and IndexedDB

[IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API)  was used to store the user's movies that he wanted to watch later. The DB has to be loaded on app start with its state provided through a context.

```ts
export interface IDBContext {
	DB: IDBDatabase | null;
	DBError: boolean;
}

export const DBContext = React.createContext<IDBContext>({
	DB: null,
	DBError: false,
});

const App: React.FC = () => {
	const { DB, hasError: DBError } = useIndexedDB();

	return (
		<DBContext.Provider value={{ DB, DBError }}>
			<Router routes={routes} fallbackRoutes={fallbackRoutes} />
		</DBContext.Provider>
	);
};
```

When the DB is setup, you access its context with the useContext hook. This allows components to react to the availability or errors of the DB and verify if DB transactions are possible. This is what it looks like with stripped out code:

```ts
export const MoviesList: React.FC<IMovieListProps> = ({
	currentRoute,
	currentRouteParams,
}) => {
    // ...
	const DBCtx = React.useContext(DBContext);

	React.useEffect(() => {
		if (DBCtx.DB === null || DBCtx.DBError) {
			return;
		}

		IDBTransaction(DBCtx.DB, {
			type: LOAD_WATCH_LATER,
			payload: null,
			callback: (movies: IMovie[]) => {
				setWatchLaterMovies({ type: ADD_WATCH_LATER, payload: movies });
			},
		});
	}, [DBCtx, setWatchLaterMovies]);


	const setMovieInDB = (action: WatchLaterActionTypes) => {
		if (DBCtx.DB === null || DBCtx.DBError) {
			console.error("Invalid IndexedDB");
			return;
		}

		IDBTransaction(DBCtx.DB, action);
	};

    // ...

	return (
		<>
            // ...
		    {hasError() && <p>An error has occured 😵</p>}
            // ...
		</>
	);
};

```

### useReducer and useCallback

useReducer is a different version of useState that feels like using the Redux library. It was used to manage the actions of adding to the watch later DB. I believe this hook makes the use of Redux obsolete for many projects, so yay for one less external dependency!

```ts
const reducer = (state: IMovie[], action: WatchLaterActionTypes) => {
	switch (action.type) {
		case ADD_WATCH_LATER:
			if (Array.isArray(action.payload)) {
				return [...state, ...action.payload];
			} else {
				return [...state, action.payload];
			}
		case REMOVE_WATCH_LATER:
			return state.filter(m => {
				if (m.id === action.payload.id) {
					return false;
				}
				return true;
			});
		default:
			throw new Error("Invalid watch later action");
	}
};

export const useWatchLater = (): UseWatchLaterReturnType => {
	const [movies, dispatch] = React.useReducer(reducer, []);

	return [movies, dispatch];
};
```

useCallback was used to manage the searching of movies. It's basically a way to change the reference of the callback passed if the chosen dependencies change. In this case, it was the debouncing state, the query, and the current page of the search.

```ts
// ...
const searchCallback = React.useCallback(
    (keepPrevData = false) => { ... },
    [debouncing, qry, currentPage]
);

// ...

React.useEffect(() => {
    // ...
}, [qry, currentPage, searchCallback]);
```

## On custom routing

The browser history API allows you to dynamically change the path of the app. It was used to determine how difficult it is to not use React Router or similar libs (it's easy). In the app, the routes were stored in a ```Map``` with the route path as key and the component to render as value:

```ts
const routes = new Map<string, RouteValue>();
routes.set("/search", {
	name: "Search",
	/* eslint-disable react/display-name */
	render: params => (
		<MoviesList currentRoute={"/search"} currentRouteParams={params} />
	),
	omitFromNavbar: true,
});
```

This map is then passed to a top-level component that can share its state with child components. If this approach is used, keep in mind that maps are passed by reference, which means you can't expect React to understand the map changed without some extra work.

## On custom configs

It was all good except for webpack. It is very annoying to setup a custom webpack config. From now on, I am just going to copy paste previous webpack configs and never write one from scratch. I would however avoid using "hidden" webpack configs, as it is the case with the ```create-react-app``` tool. If you are curious, I split the config in 3 files, ```webpack.common.js``` which is shared among the two others ```webpack.dev.js``` and ```webpack.prod.js``` with ```webpack-merge```. The difference, in this app's case, between prod and dev is only with the optimization parameter (specifically minimizing).